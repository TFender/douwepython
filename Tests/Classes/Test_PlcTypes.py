import unittest
from typing import List

from App.DTO.BlockType import BlockType
from App.Classes.PlcTypes import PlcTypes


class Test_PlcTypes(unittest.TestCase):
    whiteListBlockTypes: List[BlockType] = [BlockType.UNDEFINED, BlockType.NAI, BlockType.NI]

    def test_bit_ranges_cannot_overlap_except_whitelisted_types(self) -> None:
        for t in PlcTypes.list:
            for q in PlcTypes.list:
                if t is q or t.blockType in self.whiteListBlockTypes or q.blockType in self.whiteListBlockTypes:
                    continue

                if t.allowed_range_from == q.allowed_range_from or t.allowed_range_from == q.allowed_range_to:
                    message = "{T} allowed_range_from is equal to {Q} from or to".format(T=t.blockType, Q=q.blockType)
                    assert False, message

                if t.allowed_range_to == q.allowed_range_to or t.allowed_range_to == q.allowed_range_from:
                    message = "{T} allowed_range_to is equal to {Q} from or to".format(T=t.blockType, Q=q.blockType)
                    assert False, message

                if q.allowed_range_from < t.allowed_range_from < q.allowed_range_to:
                    message = "{T} allowed_range_from falls in range of {Q}".format(T=t.blockType, Q=q.blockType)
                    assert False, message

                if q.allowed_range_from < t.allowed_range_to < q.allowed_range_to:
                    message = "{T} allowed_range_to falls in range of {Q}".format(T=t.blockType, Q=q.blockType)
                    assert False, message

    def test_whitlist_bit_ranges_should_be_the_same(self) -> None:
        allowed_range_from = 0
        allowed_range_to = 850

        for t in PlcTypes.list:
            if t.blockType in self.whiteListBlockTypes:
                assert t.allowed_range_from == allowed_range_from, "{T} allowed_range_from is {Ta} and should be {a}".format(T=t.blockType, Ta=t.allowed_range_from, a=allowed_range_from)
                assert t.allowed_range_to == allowed_range_to, "{T} allowed_range_to is {Ta} and should be {a}".format(T=t.blockType, Ta=t.allowed_range_to, a=allowed_range_to)