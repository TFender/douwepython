import os
import unittest
from unittest.mock import Mock

from App.Services.Logger import Logger
from Utilities import Path


class Test_Logger(unittest.TestCase):
    config_service_mock = Mock()
    total_log_file_path: str

    def test_logs_should_end_up_in_logfile(self) -> None:
        log_filename = "test.log"
        log_level = "DEBUG"
        log_line = "TESTLINE!"

        self.config_service_mock.get_log_filename.return_value = log_filename
        self.config_service_mock.get_log_level.return_value = log_level

        test_logger = Logger(self.config_service_mock)
        test_logger.LogDebug(log_line)

        self.total_log_file_path = Path.get_file_path(log_filename)
        self.assertTrue(os.path.isfile(self.total_log_file_path))

        file = open(self.total_log_file_path, "r")
        line = file.readline()
        file.close()

        self.assertTrue(line.__contains__(log_line))

    def doCleanups(self) -> None:
        try:
            os.remove(self.total_log_file_path)
        except:
            print("Unable to cleanup testfile")