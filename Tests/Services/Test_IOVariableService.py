import unittest
from typing import List
from unittest.mock import Mock

from App.Classes.IOVariable import IOVariable
from App.Services.IOVariableService import IOVariableService


class Test_IOVariableService(unittest.TestCase):
    io_variable_service: IOVariableService

    logger_mock = Mock()
    data_import_service = Mock()

    def test_data_import_is_called(self) -> None:
        # arrange
        empty_list: List[IOVariable] = []
        self.data_import_service.get_data.return_value = empty_list
        self.io_variable_service = IOVariableService(self.logger_mock, self.data_import_service)

        # act
        io_variables = self.io_variable_service.get_all_variables()

        # assert
        self.data_import_service.get_data.assert_called_with()
