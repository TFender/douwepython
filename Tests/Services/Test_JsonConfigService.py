import unittest

from App.Services.JsonConfigService import JsonConfigService


class Test_JsonConfigService(unittest.TestCase):
    config_service = JsonConfigService()

    app_folder = "App"
    config_folder = "Config"

    ip_address = "192.168.0.4"
    tsap_server = 0x2000
    tsap_client = 0x2000

    variablefilename = "IO list annealing setup.xlsx"
    variablefoldername = "Resources"

    def test_get_ip_address(self):
        self.assertEqual(self.ip_address, self.config_service.get_plc_ip_address())

    def test_get_tsap_server(self):
        self.assertEqual(self.tsap_server, self.config_service.get_tsap_server())

    def test_get_tsap_client(self):
        self.assertEqual(self.tsap_client, self.config_service.get_tsap_client())

    def test_get_variable_filename(self):
        self.assertEqual(self.variablefilename, self.config_service.get_variable_filename())

    def test_get_variable_foldername(self):
        self.assertEqual(self.variablefoldername, self.config_service.get_variable_folder_name())
