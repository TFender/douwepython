import unittest
from unittest.mock import Mock

from App.Services.ExcelDataImportService import ExcelDataImportService


class Test_ExcelVariableService(unittest.TestCase):
    excel_variable_service: ExcelDataImportService

    logger_mock = Mock()
    config_service_mock = Mock()

    def test_import_of_excel_file(self) -> None:
        # arrange
        filename = "IO list annealing setup.xlsx"
        folderName = "Resources"

        self.config_service_mock.get_variable_folder_name.return_value = folderName
        self.config_service_mock.get_variable_filename.return_value = filename
        self.excel_variable_service = ExcelDataImportService(self.logger_mock, self.config_service_mock)

        # act
        io_variables = self.excel_variable_service.get_data()

        # assert
        self.assertIsNotNone(io_variables)
        self.assertTrue(len(io_variables) > 0)

