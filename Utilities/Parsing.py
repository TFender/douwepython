def parse_to_int(value) -> int:
    if value is int:
        return value

    if "0x" in value:
        return int(value, base=16)
    if "0o" in value:
        return int(value, base=8)
    if "0b" in value:
        return int(value, base=2)
    return int(value)
