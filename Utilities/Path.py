import os


def get_base_path():
    return os.path.dirname(os.path.dirname(__file__))


def get_file_path(*args: str) -> str:
    combined_args = [get_base_path()]
    for arg in args:
        combined_args.append(arg)
    return os.path.join(*combined_args)
