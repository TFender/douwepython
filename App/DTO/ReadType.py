from enum import Enum


class ReadType(Enum):
    READ_ONLY = "readonly"
    READ_WRITE = "readwrite"