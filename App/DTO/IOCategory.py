from enum import Enum


class IOCategory(Enum):
    Name = 0
    Connector = 1
    Address = 2
    Type = 3
    Signal = 4
    Range = 5
    Instrument = 6
    Comments = 7



