from enum import Enum


class BlockType(Enum):
   I = "I"
   AI = "AI"
   Q = "Q"
   AQ = "AQ"
   M = "M"
   AM = "AM"
   NQ = "NQ"
   NAQ = "NAQ"
   NI = "NI"
   NAI = "NAI"
   UNDEFINED = "undefined"