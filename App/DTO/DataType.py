from enum import Enum


class DataType(Enum):
    ANALOG = "analog"
    DIGITAL = "digital"
