from App.DTO.BlockType import BlockType
from App.DTO.DataType import DataType
from App.DTO.ReadType import ReadType


class PlcType:

    def __init__(self, blocktype: BlockType, datatype: DataType, readtype: ReadType, allowed_range_from: int,
                 allowed_range_to: int):
        self.blockType = blocktype
        self.dataType = datatype
        self.readType = readtype
        self.allowed_range_from = allowed_range_from
        self.allowed_range_to = allowed_range_to

    def __str__(self):
        return "PlcType: {B} | {D} | {R} | allowed range from : {arf}, to: {art}".format(
            B=self.blockType,
            D=self.dataType,
            R=self.readType,
            arf=self.allowed_range_from,
            art=self.allowed_range_to
        )
