from App.Classes.PlcTypes import PlcTypes
from App.DTO.IOCategory import IOCategory
from App.Classes.PlcType import PlcType


class IOVariable:
    def __init__(self):
        self.plc_type: PlcType = None
        self.fields = dict()
        for i in range(len(IOCategory)):
            self.fields[IOCategory(i)] = ""

    def add_field(self, io_category: IOCategory, value: str):
        self.fields[io_category] = value
        if io_category is IOCategory.Type:
            self.__get_number_and_plc_type(value)

    def __get_number_and_plc_type(self, field_type: str):
        for plctype in PlcTypes.list:
            if plctype.blockType.value == field_type:
                self.plc_type = plctype

        if self.plc_type is None:
            print("no fitting plc_type found for: {p}".format(p=self.__str__()))
            undefined_plc_type = PlcTypes.list[-1]
            self.plc_type = undefined_plc_type

    def __str__(self):
        return "Variable | N: {N} | C: {C} | A: {A} | T: {T} | S: {S} | R: {R} | I: {I} | Co: {Co}".format(
            N=self.fields[IOCategory.Name],
            C=self.fields[IOCategory.Connector],
            A=self.fields[IOCategory.Address],
            T=self.fields[IOCategory.Type],
            S=self.fields[IOCategory.Signal],
            R=self.fields[IOCategory.Range],
            I=self.fields[IOCategory.Instrument],
            Co=self.fields[IOCategory.Comments]
        )
