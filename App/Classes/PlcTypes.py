from typing import List

from App.DTO.BlockType import BlockType
from App.DTO.DataType import DataType
from App.Classes.PlcType import PlcType
from App.DTO.ReadType import ReadType


class PlcTypes:

    list: List[PlcType] = [
        PlcType(BlockType.I, DataType.DIGITAL, ReadType.READ_ONLY, 1024, 1031),
        PlcType(BlockType.AI, DataType.ANALOG, ReadType.READ_ONLY, 1032, 1063),
        PlcType(BlockType.Q, DataType.DIGITAL, ReadType.READ_ONLY, 1064, 1071),
        PlcType(BlockType.AQ, DataType.ANALOG, ReadType.READ_ONLY, 1072, 1103),
        PlcType(BlockType.M, DataType.DIGITAL, ReadType.READ_ONLY, 1104, 1117),
        PlcType(BlockType.AM, DataType.ANALOG, ReadType.READ_ONLY, 1118, 1245),
        PlcType(BlockType.NQ, DataType.DIGITAL, ReadType.READ_ONLY, 1390, 1405),
        PlcType(BlockType.NAQ, DataType.ANALOG, ReadType.READ_ONLY, 1406, 1469),
        PlcType(BlockType.NI, DataType.DIGITAL, ReadType.READ_WRITE, 0, 850),
        PlcType(BlockType.NAI, DataType.ANALOG, ReadType.READ_WRITE, 0, 850),
        PlcType(BlockType.UNDEFINED, DataType.ANALOG, ReadType.READ_WRITE, 0, 850)
    ]

