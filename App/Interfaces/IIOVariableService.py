from abc import abstractmethod
from typing import Protocol, List

from App.Classes.IOVariable import IOVariable


class IIOVariableService(Protocol):

    @abstractmethod
    def add_to_list(self, io_variable: IOVariable) -> None:
        ...

    @abstractmethod
    def get_all_variables(self) -> List[IOVariable]:
        ...