from abc import abstractmethod
from typing import Protocol

class IPlcConnectionService(Protocol):

    @abstractmethod
    def is_connected(self) -> bool:
        ...

    @abstractmethod
    def connect(self) -> bool:
        ...

    @abstractmethod
    def disconnect(self) -> bool:
        ...
