from abc import abstractmethod
from typing import Protocol, List

from App.Classes.IOVariable import IOVariable


class IDataImportService(Protocol):

    @abstractmethod
    def get_data(self) -> List[IOVariable]:
        ...



