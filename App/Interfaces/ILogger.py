from abc import abstractmethod
from typing import Protocol

class ILogger(Protocol):

    @abstractmethod
    def LogInformation(self, message) -> None:
        ...

    @abstractmethod
    def LogDebug(self, message) -> None:
        ...

    @abstractmethod
    def LogWarning(self, message) -> None:
        ...

    @abstractmethod
    def LogError(self, message) -> None:
        ...

    @abstractmethod
    def LogCritical(self, message) -> None:
        ...

    @abstractmethod
    def LogFatal(self, message) -> None:
        ...
