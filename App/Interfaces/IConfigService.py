from abc import abstractmethod
from typing import Protocol


class IConfigService(Protocol):

    @abstractmethod
    def get_plc_ip_address(self) -> str:
        ...

    @abstractmethod
    def get_tsap_server(self) -> int:
        ...

    @abstractmethod
    def get_tsap_client(self) -> int:
        ...

    @abstractmethod
    def get_variable_filename(self) -> str:
        ...

    @abstractmethod
    def get_variable_folder_name(self) -> str:
        ...

    @abstractmethod
    def get_log_filename(self) -> str:
        ...

    @abstractmethod
    def get_log_level(self) -> str:
        ...
