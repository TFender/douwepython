from typing import List

import pandas as pd

from App.Classes.IOVariable import IOVariable
from App.DTO.IOCategory import IOCategory
from App.Interfaces.IConfigService import IConfigService
from App.Interfaces.IDataImportService import IDataImportService
from App.Interfaces.ILogger import ILogger
from Utilities import Path


class ExcelDataImportService(IDataImportService):
    variables: List[IOVariable] = None

    def __init__(self, logger: ILogger, config_service: IConfigService):
        self.logger = logger
        self.configService = config_service


    def get_data(self) -> List[IOVariable]:
        if self.variables is None:

            excel_file_path = self.__get_excel_file_path()
            data = self.__import_data_from_excel_file(excel_file_path)

            self.variables = []
            for i in range(len(data)):
                data_row = data[i]
                io_variable = self.__convert_data_row_to_io_variable(data_row)
                self.variables.append(io_variable)

        self.logger.LogInformation("Data imported...")

        return self.variables

    def __get_excel_file_path(self) -> str:
        return Path.get_file_path(self.configService.get_variable_folder_name(),
                                  self.configService.get_variable_filename())

    def __import_data_from_excel_file(self, excelfilepath):
        df = pd.read_excel(excelfilepath)
        self.__check_for_validity_of_columns(df.columns)
        return df.to_dict("index")

    def __check_for_validity_of_columns(self, columns) -> None:
        if len(IOCategory) != len(columns):
            raise IOError("Amount of columns not correct in file! expected: {e}, actual: {a}".format(
                e=len(IOCategory), a=len(columns)))

        for i in range(len(columns)):
            if columns[i] != IOCategory(i).name:
                raise IOError("Incorrect column name in file! expected: {e}, actual {a}".format(
                    e=IOCategory(i).name, a=columns[i]))

    def __convert_data_row_to_io_variable(self, row) -> IOVariable:
        io_variable = IOVariable()

        for i in range(len(IOCategory)):
            if IOCategory(i) is not IOCategory.Type:
                self.__add_field_to_io_variable(row, io_variable, IOCategory(i))

        self.__add_field_to_io_variable(row, io_variable, IOCategory.Type)

        return io_variable

    def __add_field_to_io_variable(self, row, io_variable: IOVariable, io_category: IOCategory):
        field = row[io_category.name]
        io_variable.add_field(io_category, field)