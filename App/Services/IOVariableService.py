from typing import List
from App.Classes.IOVariable import IOVariable
from App.DTO.IOCategory import IOCategory
from App.Interfaces.IDataImportService import IDataImportService
from App.Interfaces.IIOVariableService import IIOVariableService
from App.Interfaces.ILogger import ILogger


class IOVariableService(IIOVariableService):
    __list: List[IOVariable] = None

    def __init__(self, logger: ILogger, data_import_service: IDataImportService):
        self.logger = logger
        self.data_import_service = data_import_service

    def add_to_list(self, io_variable: IOVariable):
        fields = io_variable.fields

        if fields[IOCategory.Type] is None:
            raise ValueError(
                "Missing field Type in variable: {io_var}".format(io_var=io_variable.__str__()))
        if fields[IOCategory.Connector] is None:
            raise ValueError(
                "Missing field Connector in variable: {io_var}".format(io_var=io_variable.__str__()))
        if fields[IOCategory.Address] is None:
            raise ValueError(
                "Missing field Address in variable: {io_var}".format(io_var=io_variable.__str__()))

        self.__list.append(io_variable)

    def get_all_variables(self) -> List[IOVariable]:
        if self.__list is None:
            self.__list = []
            data = self.data_import_service.get_data()
            for io_variable in data:
                self.logger.LogInformation("Data Row: {D}".format(D=io_variable))
                self.add_to_list(io_variable)

        return self.__list.copy()
