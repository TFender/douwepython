from App.Interfaces.IConfigService import IConfigService
from App.Interfaces.ILogger import ILogger
from App.Interfaces.IPlcConnectionService import IPlcConnectionService


class PlcConnectionService(IPlcConnectionService):

    def __init__(self, logger: ILogger, config: IConfigService):
        self.config = config

    def is_connected(self) -> bool:
        pass

    def connect(self) -> bool:
        pass

    def disconnect(self) -> bool:
        pass
