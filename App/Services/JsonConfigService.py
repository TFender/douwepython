import json
import os

from App.Interfaces.IConfigService import IConfigService
from Utilities import Path
from Utilities import Parsing


class JsonConfigService(IConfigService):

    config_data = None
    config_path = None

    app_folder = "App"
    config_folder = "Config"
    config_file_name = "config.json"

    def __init__(self):
        self.__set_config_path()
        self.__load_config_data()

    def get_variable_filename(self) -> str:
        return self.__get_variable('VariableResource', 'Filename')

    def get_variable_folder_name(self) -> str:
        return self.__get_variable('VariableResource', 'FolderName')

    def get_plc_ip_address(self) -> str:
        return self.__get_variable('PlcConfig', 'IpAddress')

    def get_tsap_server(self) -> int:
        data = self.__get_variable('PlcConfig', 'tsap_server')
        return self.__variable_to_int(data)

    def get_tsap_client(self) -> int:
        data = self.__get_variable('PlcConfig', 'tsap_client')
        return self.__variable_to_int(data)

    def get_log_filename(self) -> str:
        return self.__get_variable('Logging', 'Filename')

    def get_log_level(self) -> str:
        return self.__get_variable('Logging', 'Level')

    def __variable_to_int(self, data) -> int:
        return Parsing.parse_to_int(data)

    def __set_config_path(self) -> None:
        path = Path.get_base_path()
        self.config_path = os.path.join(path, self.app_folder, self.config_folder, self.config_file_name)

    def __load_config_data(self) -> None:
        with open(self.config_path) as json_file:
            self.config_data = json.load(json_file)

    def __get_variable(self, *args: str):
        variable = None
        for arg in args:
            if variable is None:
                variable = self.config_data[arg]
            else:
                variable = variable[arg]
        return variable
