import logging
import sys

from App.Interfaces.IConfigService import IConfigService
from App.Interfaces.ILogger import ILogger
from Utilities import Path



class Logger(ILogger):

    def LogCritical(self, message) -> None:
        self.logger.critical(message)

    def LogFatal(self, message) -> None:
        self.logger.fatal(message)

    def LogDebug(self, message) -> None:
        self.logger.debug(message)

    def LogWarning(self, message) -> None:
        self.logger.warning(message)

    def LogError(self, message) -> None:
        self.logger.error(message)

    def LogInformation(self, message) -> None:
        self.logger.info(message)

    def __init__(self, config_service: IConfigService):
        file_name = config_service.get_log_filename()
        total_file_path = Path.get_file_path(file_name)

        log_level = config_service.get_log_level()
        log_format = '%(asctime)-15s %(levelname)-4s: %(message)s'
        log_level_enum_value = self.__get_log_level_int(log_level)

        logging.basicConfig(filename=total_file_path, format=log_format)

        self.logger = logging.getLogger()
        self.logger.setLevel(log_level_enum_value)

        def log_uncaught_exceptions(ex_cls, ex, tb):
            logging.error("Uncaught exception", exc_info=(ex_cls, ex, tb))

        sys.excepthook = log_uncaught_exceptions

    def __get_log_level_int(self, level: str) -> int:
        switcher = {
            "CRITICAL": 50,
            "FATAL": 50,
            "ERROR": 40,
            "WARNING": 30,
            "WARN": 30,
            "INFO": 20,
            "DEBUG": 10,
            "NOTSET": 0,
        }
        return switcher.get(level, 0)
