from App.Interfaces import IConfigService
from App.Interfaces.IDataImportService import IDataImportService
from App.Interfaces.IIOVariableService import IIOVariableService
from App.Interfaces.ILogger import ILogger
from App.Interfaces.IPlcConnectionService import IPlcConnectionService
from App.Services.ExcelDataImportService import ExcelDataImportService
from App.Services.IOVariableService import IOVariableService
from App.Services.JsonConfigService import JsonConfigService
from App.Services.Logger import Logger
from App.Services.PlcConnectionService import PlcConnectionService



def register_instances():

    # This is what a Dependency Injection Container should do!
    config_service: IConfigService = JsonConfigService()
    logger: ILogger = Logger(config_service)

    data_import_service: IDataImportService = ExcelDataImportService(logger, config_service)
    io_variable_service: IIOVariableService = IOVariableService(logger, data_import_service)
    plc_connection_service: IPlcConnectionService = PlcConnectionService(logger, config_service)

    io_variable_service.get_all_variables()

if __name__ == '__main__':
    print("Starting Application")

    register_instances()

